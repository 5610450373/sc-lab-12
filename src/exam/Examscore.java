package exam;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class Examscore {
	FileWriter fileWriter = null;
	
	public double cal_mean(double[] a){
		double n = a.length;
		double sum=0;
		for(double nn : a){
			sum += nn;
		}
		double mean = sum/n;
		return mean;
	}
	
	public void write(String filename,ArrayList<String> val){
		try {	
			fileWriter = new FileWriter(filename,true);
			BufferedWriter out = new BufferedWriter(fileWriter);
			out.write("____HOMEWORK SCORE____");
			out.newLine();
			out.write("Name Average");
			out.newLine();
			System.out.println("____Exam SCORE____");
			System.out.println("Name Average");
			for(String s : val){
				out.write(""+s);
				out.newLine();
				System.out.println(s);
			}
			out.flush();
		}
		catch (IOException e){
			System.err.println("Error reading from user");
		 }
	}
	
	public ArrayList<String> read(String filename){
		ArrayList<String> lis = new ArrayList<String>();
		try{
			FileReader fileReader = new FileReader(filename);
			BufferedReader buffer = new BufferedReader(fileReader);
			String line = buffer.readLine();
			while (line != null) {
				String[] s = line.split(",");
				double[] val = new double[s.length-1];
				String st="";
				for(int i=0;i<s.length;i++){
					if(i==0){
						st +=s[i];
					}
					else{
						val[i-1] = Double.parseDouble(s[i]);
					}
				}
				double m = cal_mean(val);
				st+=" "+m;
				lis.add(st);
				line = buffer.readLine();
			 }
	 	 }
	 	 catch (IOException e){
			 System.err.println("Error reading from file");
	 	 }
		return lis;
	}
}
